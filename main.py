from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from threading import Thread
import pandas as pd
import random
import time
import os

class Scrapper:
    # Add proxies to this if you want to use proxies
    proxies = []

    def __init__(self, file):
        # read the data storage file
        #self.df = pd.read_csv(file)
        self.LOG = False

        # Create a directory to store the files
        try:
            os.mkdir("data")
        except:
            print "[LOG] Folder with name data already exits."

        
        # TODO Write code which continues from last run


    def setup_driver(self, proxy=""):

        # TODO add support for using proxies
        #
        #

        return webdriver.Chrome(executable_path=r"./chromedriver")
    
    def parse_data(self):

        # Get every field
        fields = self.driver.find_elements_by_xpath('//*[@id="app"]/div[4]/div/div[2]/div[1]/div[2]/div[1]/div/div/div')
        
        for field in fields:
            # Get the years
            years = field.find_elements_by_xpath("//div/div/select/option")
            print years

    def run(self):
        # Basically run from a set of URLS and then parse for the data
        # TODO Read URLs from a file
        URLS = [
                "https://www.realtruck.com/rugged-ridge-floor-mats",
                "https://www.realtruck.com/superlift-steering-stabilizers",
                "https://www.realtruck.com/nitto-420s-all-season-tires/",
                "https://www.realtruck.com/putco-halogen-headlight-bulbs/",
                "https://www.realtruck.com/husky-liners-weatherbeater-floor-liners",
                "https://www.realtruck.com/curt-q-series-5th-wheel-hitch/"
                ]

        # TODO provide support all the URL types
        _urls = [URLS[0]]

        self.driver = self.setup_driver()

        for url in _urls:
            self.driver.get(url)
            self.parse_data()

def main():
    sc = Scrapper("extract.csv")
    sc.run()

main()
